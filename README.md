## How to run
1. Download the datasets, extract them into **datasets** directory
2. Create a new python **venv** using the command: `python3 -m venv env`
3. Activate the new virtual environment: `env/Scripts/activate.bat`
4. Install the required python packages: `pip install -r requrements.txt`
5. Run `jupyter notebook --NotebookApp.max_buffer_size=[your_value]` where **your_value** is the allocated memory in bytes (I used 16Gib which is 17179869184 bytes)
6. Make sure to update `TOXIC_COMMENTS_DIR` and `MUSIC_DIR` variables if needed

## References

Pereira, R. B., Plastino, A., Zadrozny, B., & Merschmann, L. H. (2018). Correlation analysis of performance measures for multi-label classification. Information Processing & Management, 54(3), 359-369. 

Toxic comments dataset: https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge/data  

Multilabel classification music emotions dataset: https://www.kaggle.com/srinivas365/multilabel-classification-emotions 

